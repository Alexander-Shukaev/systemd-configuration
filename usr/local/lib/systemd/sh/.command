#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file .command
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-06-25 Friday 20:09:06 (+0200)
##  --------------------------------------------------------------------------
##     @created 2020-05-22 Friday 15:04:25 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Input Field Separators {{{
##  ==========================================================================
IFS=' ''	''
'
##  ==========================================================================
##  }}} Input Field Separators
##
### Built-Ins {{{
##  ==========================================================================
{
  \unalias -a         || :
  \unset   -f command || :
} 1> '/dev/null' 2>&1
if [ -n "${ZSH_VERSION-}" ]; then
  options[POSIX_BUILTINS]=on
fi
##  ==========================================================================
##  }}} Built-Ins
##
### Options {{{
##  ==========================================================================
set -e
set -u
if [ -n "${BASH-}" ] || [ -n "${BASH_VERSION-}" ]; then
  set +E
  set +o pipefail
  set -o posix
fi
if [ -n "${ZSH_VERSION-}" ]; then
  emulate sh
fi
##  ==========================================================================
##  }}} Options
##
### Executable Search Paths {{{
##  ==========================================================================
CS_PATH="$(command -p getconf CS_PATH || :)"
   PATH="${PATH}${CS_PATH:+${PATH:+:}${CS_PATH}}"
##  ==========================================================================
##  }}} Executable Search Paths
##
### Internal Functions {{{
##  ==========================================================================
__basename__() {
  command echo "${1##*/}"
}
##
__reallink__() (
  link="${1}"
  path="${1}"
  [ -n "${path}" ] || return "${?}"
  while [ -L "${path}" ]; do
    dir="${path%/*}"
    if [ "${dir}" = "${path}" ]; then
      dir='.'
    fi
    CDPATH= command cd -- "${dir}/" || return "${?}"
    dir="$(command pwd -P)" || return "${?}"
    CDPATH= command cd -- "${dir}/" || return "${?}"
    link="${path##*/}"
    path="$(command readlink "${link}")" || return "${?}"
  done
  dir="${link%/*}"
  if [ "${dir}" = "${link}" ]; then
    dir='.'
  fi
  CDPATH= command cd -- "${dir}/" || return "${?}"
  dir="$(command pwd -P)" || return "${?}"
  dir="${dir%/}"
  link="${link##*/}"
  if   [ "${link}" = '.'  ]; then
    link=
  elif [ "${link}" = '..' ]; then
    dir="${dir%/*}"
    link=
  fi
  command echo "${dir:-/}${link:+${dir:+/}}${link}"
)
##
__realpath__() (
  path="${1}"
  [ -n "${path}" ] || return "${?}"
  while [ -L "${path}" ]; do
    dir="${path%/*}"
    if [ "${dir}" = "${path}" ]; then
      dir='.'
    fi
    CDPATH= command cd -- "${dir}/" || return "${?}"
    dir="$(command pwd -P)" || return "${?}"
    CDPATH= command cd -- "${dir}/" || return "${?}"
    path="${path##*/}"
    path="$(command readlink "${path}")" || return "${?}"
  done
  dir="${path%/*}"
  if [ "${dir}" = "${path}" ]; then
    dir='.'
  fi
  CDPATH= command cd -- "${dir}/" || return "${?}"
  dir="$(command pwd -P)" || return "${?}"
  dir="${dir%/}"
  path="${path##*/}"
  if   [ "${path}" = '.'  ]; then
    path=
  elif [ "${path}" = '..' ]; then
    dir="${dir%/*}"
    path=
  fi
  command echo "${dir:-/}${path:+${dir:+/}}${path}"
)
##  ==========================================================================
##  }}} Internal Functions
##
### Internal Variables {{{
##  ==========================================================================
       __link__="${SYSTEMD_SOURCE:-${0}}"
   __reallink__="$(__reallink__           "${__link__}")"
__reallinkdir__="$(command dirname -- "${__reallink__}")"
##
   __realfile__="$(__realpath__       "${__reallink__}")"
__realfiledir__="$(command dirname -- "${__realfile__}")"
##
       __file__="${__reallinkdir__%/}/${__realfile__##*/}"
    __relfile__="${__file__#${__realfiledir__%/}/}"
##
   __REALLINK__="${__reallink__}"
__REALLINKDIR__="${__reallinkdir__}"
##
       __FILE__="${__realfiledir__%/}/.sources.d/${__relfile__#/}"
   __REALFILE__="$(__realpath__           "${__FILE__}")"
__REALFILEDIR__="$(command dirname -- "${__REALFILE__}")"
##  ==========================================================================
##  }}} Internal Variables
##
### Checks {{{
##  ==========================================================================
if [ ! -L "${__link__}" ]; then
  printf -- '%s: %s: %s\n' 'error' "${__link__}" 'No such symbolic link' 1>&2
  exit 1
fi
if [  "${__relfile__}" =     "${__file__}" ] ||                              \
   [ "${__realfile__}" =     "${__FILE__}" ] ||                              \
   [ "${__realfile__}" = "${__REALFILE__}" ]; then
  printf -- '%s: %s: %s\n' 'error' "${__link__}" 'Invalid symbolic link' 1>&2
  exit 1
fi
if [ "${SYSTEMD_TARGET-}" = "${__REALFILE__}" ]; then
  printf -- '%s: %s: %s\n' 'error' "${__link__}" 'Recursive execution'   1>&2
  exit 1
fi
##  ==========================================================================
##  }}} Checks
##
### Variables {{{
##  ==========================================================================
SYSTEMD_SOURCE="${__FILE__}"
SYSTEMD_TARGET="${__REALFILE__}"
##  ==========================================================================
##  }}} Variables
##
### Exports {{{
##  ==========================================================================
export SYSTEMD_TARGET
##  ==========================================================================
##  }}} Exports
##
### Sources {{{
##  ==========================================================================
. "${__REALFILE__}"
##  ==========================================================================
##  }}} Sources
