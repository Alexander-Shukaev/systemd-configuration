#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file systemd-mail.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-12-26 Sunday 17:36:43 (+0100)
##  --------------------------------------------------------------------------
##     @created 2021-06-27 Sunday 16:37:06 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##
CMD="$(delimiter=' ' join "${0}" "${@}")"
##
systemd_user=
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
systemd_invocation_id() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  set -- --property=InvocationID --value "${@}"
  systemctl ${systemd_user:+--user} show "${@}"
}
##
systemd_timeout() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  if [ "${#}" -gt 0 ]; then
    set -- --property=TimeoutStopUSec        --value "${@}"
  else
    set -- --property=DefaultTimeoutStopUSec --value
  fi
  systemctl ${systemd_user:+--user} show "${@}"
}
##
systemd_journal_log() {
  __quote__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -q|--quote)
        __quote__=1
        ;;
      --)
        shift
        break
        ;;
      -?*)
        e '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  set -- "INVOCATION_ID=${1}"
  set -- "${systemd_user:+USER_}${1}" + "_SYSTEMD_${1}"
  set -- journalctl --no-hostname -o short-iso-precise -q "${@}"
  {
    "${@}" | {
      set --                                                                 \
          '[0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}\(\.[0-9]*\)\{0,1\}' '+[0-9]\{4\}'
      sed ${__quote__:+-e 's/^/> /'} -e 's/\('"${1}"'\)'"${2}"'/\1/'
    }
  } && set -- "${?}" || set -- "${?}"
  unset -v __quote__
  return "${1}"
}
##
systemd_status() {
  __quote__=
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -q|--quote)
        __quote__=1
        ;;
      --)
        shift
        break
        ;;
      -?*)
        e '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  set -- systemctl ${systemd_user:+--user} status --full --lines 0 "${1}"
  {
    "${@}" | if [ -n "${SYSTEMD_MAIL_DEPRECATED-}" ]; then
      # ----------------------------------------------------------------------
      # DEPRECATED:
      #
      {
        set --                                                               \
            sed ${__quote__:+-e 's/^/> /'} -e 's/[C]ntrl PID:/Control PID:/g'
        "${@}" | grep -F -v -e "$(delimiter=' ' join "${@}")"
      } | grep -F -v -e "$(delimiter=' ' join "${@}")" ${CMD:+-e "${CMD}"}
      # ----------------------------------------------------------------------
    else
      sed ${__quote__:+-e 's/^/> /'}                                         \
          -e 's/Cntrl PID:/Control PID:/g'                                   \
          -e '/CGroup:/q'
    fi
  } && set -- "${?}" || set -- "${?}"
  unset -v __quote__
  return "${1}"
}
##
systemd_unit() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  set -- systemctl ${systemd_user:+--user} status --full --lines 0 "${1}"
  "${@}" | awk 'NR == 1 { print $2 }'
}
##
systemd_mkdir() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  for __dir__ in "${@}"; do
    if mkdir -m 0700 -p -- "${__dir__}"; then
      set -- "${?}"
      echo "${__dir__}"
    else
      set -- "${?}"
      break
    fi
  done
  unset -v __dir__
  return "${1}"
}
##
systemd_mkdtemp() {
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      --)
        shift
        break
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  mkdtemp -- "systemd-mail-${1:+${1}-}${2:+${2}-}"
}
##
systemd_mail() {
  if [ -n "${ZSH_VERSION-}" ]; then
    emulate -L sh
  fi
  __attach_log__=
  __background__=
        __host__=
         __iid__=
     __no_fork__=1
       __quiet__=
     __subject__=
     __summary__='Starting'
   __s_summary__=
   __f_summary__=
     __failure__=
  __to_address__=
   __to_domain__=
  __to_mailbox__=
        __user__=
        __wait__='infinity'
        __unit__=
     __pw_file__="$(xdg_config_home)/keyrings/login.password" || return "${?}"
  case "${SERVICE_RESULT-}" in
    success)
      __summary__='Finished'
      ;;
    ?*)
      __summary__='Failure'
      ;;
  esac
  while [ "${#}" -gt 0 ]; do
    case "${1}" in
      -al|--attach-log)
        __attach_log__=1
        ;;
      -b|--background|-d|--daemon|--daemonize)
        if [ -z "${SYSTEMD_MAIL_BACKGROUND-}" ]; then
          __background__=1
          return 0
        fi
        ;;
      -iid|--invocation-id)
        a [ "${#}" -gt 1 ] || return "${?}"
        __iid__="${2}"
        shift
        ;;
      -f|--fork)
        __no_fork__=
        ;;
      -q|--quiet)
        __quiet__=1
        ;;
      -s|--summary)
        a [ "${#}" -gt 1 ] || return "${?}"
        __summary__="${2}"
        shift
        ;;
      -ss|--success-summary)
        a [ "${#}" -gt 1 ] || return "${?}"
        __s_summary__="${2}"
        shift
        ;;
      -fs|--failure-summary)
        a [ "${#}" -gt 1 ] || return "${?}"
        __f_summary__="${2}"
        shift
        ;;
      -sf|--send-failure)
        __failure__=1
        ;;
      -ta|--to-address)
        a [ "${#}" -gt 1           ] || return "${?}"
        a [ -z  "${__to_domain__}" ] || return "${?}"
        a [ -z "${__to_mailbox__}" ] || return "${?}"
        __to_address__="${2}"
        shift
        ;;
      -td|--to-domain)
        a [ "${#}" -gt 1           ] || return "${?}"
        a [ -z "${__to_address__}" ] || return "${?}"
        __to_domain__="${2}"
        shift
        ;;
      -tm|--to-mailbox)
        a [ "${#}" -gt 1           ] || return "${?}"
        a [ -z "${__to_address__}" ] || return "${?}"
        __to_mailbox__="${2}"
        shift
        ;;
      -u|--user)
        systemd_user=1
        ;;
      -w|--wait|-t|--timeout)
        a [ "${#}" -gt 1 ] || return "${?}"
        __wait__="${2#infinity}"
        shift
        ;;
      --)
        shift
        break
        ;;
      -?*)
        e '%s: %s' "${1}" 'Invalid option'
        return 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done
  case "${SERVICE_RESULT-}" in
    success)
      __summary__="${__s_summary__:-${__summary__}}"
      ;;
    ?*)
      __summary__="${__f_summary__:-${__summary__}}"
      ;;
  esac
  __result__="${SERVICE_RESULT-}"
  __result__="${__result__#success}"
  if [ "${#}" -gt 0 ]; then
    __unit__="${1}"
    shift
  fi
  if [ -n "${__unit__}" ]; then
    if [ -n "${__iid__}" ]; then
      a [ "${__iid__}" = "$(systemd_invocation_id -- "${__unit__}" || :)" ] ||
        return "${?}"
    else
      __iid__="$(systemd_invocation_id -- "${__unit__}" || :)"
    fi
  fi
  __unit_or_iid__="${__unit__:-${__iid__}}"
  if [ "${__wait__}" = 'infinity' ]; then
    __wait__="$({
        systemd_timeout ${__unit_or_iid__:+-- "${__unit_or_iid__}"} || echo 60
      } | tr -d ' ')"
    __wait__="${__wait__#infinity}"
    if [ "${__wait__}" != "${__wait__%%[![:digit:]]*}" ]; then
      __wait__="${__wait__%${__wait__#*[![:digit:]]}}"
    fi
  fi
  __unit_or_pid__="${__unit__:-${PID}}"
  if [ -z "${__failure__}" ]; then
    __no_fork__=
  fi
  ##
  __to_mailbox__="${__to_mailbox__:-${__to_address__%@*}}"
  __to_mailbox__="${__to_mailbox__:-systemd}"
  ##
   __to_domain__="${__to_domain__:-${__to_address__##*@}}"
  ##
  __to_address__="${__to_mailbox__}${__to_domain__:+@${__to_domain__}}"
  ##
      __host__="${__host__:-$(uname -n || hostname || :)}"
      __user__="${__user__:-$(id -u -n || :)}"
  ##
   __subject__="${__user__}${__host__:+@${__host__}}"
   __subject__="${__subject__}${__unit__:+${__subject__:+:}${__unit__}}"
   __subject__="[Systemd]${__subject__:+ [${__subject__}]}"
   __subject__="${__subject__}${__result__:+ [${__result__}]}"
   __subject__="${__subject__}${__summary__:+ ${__summary__%%
*}}"
  ##
  unset -v __result__
  ##
  __sigdelim__='
-- '
  ##
   __version__="$(qif systemctl --version | awk 'NR == 1 { print $2 }')" ||
     a [ -z "${__version__}" ] || return "${?}"
       __url__='http://systemd.io/'
  ##
      __name__="Systemd${__version__:+ (${__version__})} <${__url__}>"
  ##
  (
    __mm_file__=
    __log_file__=
    rmdirr_trap_mk -- __mm_dir__                                             \
                   -- systemd_mkdtemp                                        \
                   -- ${__iid__:+"${__iid__}"} ${__unit__:+"${__unit__}"} && {
      __mm_file__="${__mm_dir__}/message"
      if [ -n "${__attach_log__}" ] && [ -n "${__iid__}" ]; then
        if q command_name -- pigz; then
          __compressor__='pigz'
        else
          __compressor__='gzip'
        fi
        __log_file__="${__mm_dir__}/${__iid__}.log.gz"
        systemd_journal_log -- "${__iid__}" |
          "${__compressor__}" -9 >| "${__log_file__}" ||
          __log_file__=
      fi
      __log__="${__log_file__##*/}"
      cat <<- __END__ >| "${__mm_file__}"
${__user__:+User: ${__user__}
}${__host__:+Host: ${__host__}
}${__unit__:+Unit: ${__unit__}
}${__iid__:+Invocation ID: ${__iid__}
}${__summary__:+Summary: ${__summary__}
}${__unit_or_pid__:+Status:
$(systemd_status -q -- "${__unit_or_pid__}")
}${__iid__:+Log: ${__log__:-
$(systemd_journal_log -q -- "${__iid__}")}
}${__sigdelim__:-
-- }
This message was sent by ${__name__}.
__END__
    } || exit "${?}"
    if [ -z "${__quiet__}" ]; then
      printf -- '%s\n' '-----BEGIN SYSTEMD MAIL MESSAGE-----'
      cat    -- "${__mm_file__}"
      printf -- '%s\n' '-----END SYSTEMD MAIL MESSAGE-----'
    fi
    (
      secret_unlock -- "${__pw_file__}" &&
        LC_ALL='C' n0 retry -i 0 ${__wait__:+-w "${__wait__}"} --            \
          ${__no_fork__:+exec --} mail -: /                                  \
                                       -S mime-force-sendout                 \
                                       -S nosave                             \
                                       -S sendwait                           \
                                       -S stealthmua=                        \
                                       -S ttycharset=utf8                    \
                                       ${__log_file__:+-a "${__log_file__}"} \
                                       -q "${__mm_file__}"                   \
                                       -s "${__subject__}"                   \
                                       -. "${__to_address__}" "${@}"
    )
  ) && set -- "${?}" || set -- "${?}"
  if [ -z "${__failure__}" ]; then
    set -- 0
  fi
  unset -v __attach_log__
  unset -v __background__
  unset -v       __host__
  unset -v        __iid__
  unset -v    __no_fork__
  unset -v      __quiet__
  unset -v    __subject__
  unset -v    __summary__
  unset -v  __s_summary__
  unset -v  __f_summary__
  unset -v    __failure__
  unset -v __to_address__
  unset -v  __to_domain__
  unset -v __to_mailbox__
  unset -v       __user__
  unset -v       __wait__
  unset -v       __unit__
  unset -v       __unit_or_iid__
  unset -v       __unit_or_pid__
  unset -v    __pw_file__
  unset -v   __sigdelim__
  unset -v    __version__
  unset -v        __url__
  unset -v       __name__
  return "${1}"
}
##  ==========================================================================
##  }}} Functions
##
### Variables {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Variables
##
### Commands {{{
##  ==========================================================================
a ! trapped && systemd_mail "${@}" && {
  if [ -n "${__background__-}" ]; then
    SYSTEMD_MAIL_BACKGROUND=1 a daemon "${0}" "${@}"
  fi
}
##  ==========================================================================
##  }}} Commands
