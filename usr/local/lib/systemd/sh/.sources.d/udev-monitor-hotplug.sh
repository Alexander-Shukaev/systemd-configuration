#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file udev-monitor-hotplug.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-12-18 Saturday 16:45:13 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-05-22 Friday 15:04:25 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
UDEV_CONFIG_HOME="$(xdg_config_home)/udev"
UDEV_ENVIRONMENT_FILE="${UDEV_CONFIG_HOME}/udev.conf"
##  ==========================================================================
##  }}} Variables
##
### Exports {{{
##  ==========================================================================
export UDEV_CONFIG_HOME
export UDEV_ENVIRONMENT_FILE
##  ==========================================================================
##  }}} Exports
##
### Locks {{{
##  ==========================================================================
a lock -c -d -e -p "${UDEV_CONFIG_HOME}/handlers.d" -w 11 -v -- "${0}" "${@}"
##  ==========================================================================
##  }}} Locks
##
### Traps {{{
##  ==========================================================================
a trap_or_exit -- :
##  ==========================================================================
##  }}} Traps
##
### Variables {{{
##  ==========================================================================
    event="${0##*/udev-}"
   seqnum="${1##*:}"
  devname="${1%:*}"
logprefix="udev[${event}#${seqnum}:${devname}]"
##  ==========================================================================
##  }}} Variables
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: Begin"
echo "Received event '${event}'"                                             \
     "with sequence number #${seqnum}"                                       \
     "for device '${devname}'"
##  ==========================================================================
##  }}} Messages
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##
UDEV_EVENT="${event}"
UDEV_LOGPREFIX="${logprefix}"
UDEV_SEQNUM="${seqnum}"
##
env="$(udevadm info --query='env' --export "${devname}")"
eval ${env}
a [ "${devname}" = "${DEVNAME}" ]
while IFS= read -r __var__; do
  eval   UDEV_${__var__}
  export UDEV_${__var__%%=*}
done <<- __END__
${env}
__END__
unset -v env
##  ==========================================================================
##  }}} Variables
##
### Exports {{{
##  ==========================================================================
export UDEV_EVENT
export UDEV_LOGPREFIX
export UDEV_SEQNUM
##  ==========================================================================
##  }}} Exports
##
### Messages {{{
##  ==========================================================================
echo "UDEV Environment:"
export -p | cut -d ' ' -f 2- | grep -e '^UDEV_'
echo "User Environment:"
echo "DBUS_SESSION_BUS_ADDRESS='${DBUS_SESSION_BUS_ADDRESS-}'"
echo "DISPLAY='${DISPLAY-}'"
echo "HOME='${HOME-}'"
echo "USER='${USER-}'"
echo "XAUTHORITY='${XAUTHORITY-}'"
##  ==========================================================================
##  }}} Messages
##
### Handlers {{{
##  ==========================================================================
# TODO:
#
# Similar to 'dbus-logind-monitor':
#
if [ -d "${UDEV_CONFIG_HOME}/handlers.d/" ]; then
  for _handlers in "${UDEV_CONFIG_HOME}/handlers.d/${event}.handlers"        \
                   "${UDEV_CONFIG_HOME}/handlers.d/"*"-${event}.handlers"; do
    if   [ -x "${_handlers}" ]; then
      echo "Running event handlers '${_handlers}'..."
      a "${_handlers}" || :
    elif [ -r "${_handlers}" ]; then
      echo "Reading event handlers '${_handlers}'..."
      (a . "${_handlers}" || :)
    fi
  done
  unset -v _handlers
fi
##  ==========================================================================
##  }}} Handlers
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: End"
##  ==========================================================================
##  }}} Messages
##
### References {{{
##  ==========================================================================
##  [1] file:///etc/systemd/user/udev-monitor-hotplug@.service
##  ==========================================================================
##  }}} References
