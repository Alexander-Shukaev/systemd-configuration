#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file display-setup.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-03-21 Monday 12:33:34 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-05-22 Friday 15:04:25 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Locks {{{
##  ==========================================================================
a lock -c -a -e -p "$(displaylockfile)" -w 11 -v -- "${0}" "${@}"
##  ==========================================================================
##  }}} Locks
##
### Traps {{{
##  ==========================================================================
a trap_or_exit -f -- :
##  ==========================================================================
##  }}} Traps
##
### Variables {{{
##  ==========================================================================
logprefix="Display Setup"
##  ==========================================================================
##  }}} Variables
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: Begin"
##  ==========================================================================
##  }}} Messages
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##  ==========================================================================
##  }}} Variables
##
### Messages {{{
##  ==========================================================================
echo "User Environment:"
echo "DBUS_SESSION_BUS_ADDRESS='${DBUS_SESSION_BUS_ADDRESS-}'"
echo "DISPLAY='${DISPLAY-}'"
echo "HOME='${HOME-}'"
echo "USER='${USER-}'"
echo "XAUTHORITY='${XAUTHORITY-}'"
##  ==========================================================================
##  }}} Messages
##
### Functions {{{
##  ==========================================================================
xdpyinfo_dimensions() {
  a xdpyinfo | a awk '/dimensions:/ { print $2 }'
}
##
xdpyinfo_dimensions_x() {
  set -- "$(xdpyinfo_dimensions)" "${?}"
  [ "${2}" -eq 0 ] || return "${2}"
  echo "${1%%x*}"
}
##
xdpyinfo_dimensions_y() {
  set -- "$(xdpyinfo_dimensions)" "${?}"
  [ "${2}" -eq 0 ] || return "${2}"
  echo "${1##*x}"
}
##
xdpyinfo_resolution() {
  a xdpyinfo | a awk '/resolution:/ { print $2 }'
}
##
xdpyinfo_resolution_x() {
  set -- "$(xdpyinfo_resolution)" "${?}"
  [ "${2}" -eq 0 ] || return "${2}"
  echo "${1%%x*}"
}
##
xdpyinfo_resolution_y() {
  set -- "$(xdpyinfo_resolution)" "${?}"
  [ "${2}" -eq 0 ] || return "${2}"
  echo "${1##*x}"
}
##
xrandr_is_output_status() {
  a [ -n "${1-}" ] && a [ -n "${2-}" ] || return "${?}"
  a xrandr | grep -e "${1}.*\b${2}\b" -q
}
##
xrandr_wait_output_status() {
  retry -c "${xrandr_wait_count:-10}"                                        \
        -i "${xrandr_wait_interval:-0.1}"                                    \
        --                                                                   \
        xrandr_is_output_status "${1}" "${2:-connected}"
}
##
xrandr_retry() {
  retry -c "${xrandr_retry_count:-10}"                                       \
        -i "${xrandr_retry_interval:-0.1}"                                   \
        --                                                                   \
        xrandr "${@}"
}
##
xrandr_dual_output() {
  for i in 1 2 3 4; do
    eval _${i}="\"\${${1}-}\""
    eval _${i}_status_connected="\"\${${1}_status_connected-}\""
    shift
  done
  if [ -n "${_1_status_connected}" ]   &&                                    \
     [ -n "${_2_status_connected}" ]   &&                                    \
     xrandr_wait_output_status "${_1}" &&                                    \
     xrandr_wait_output_status "${_2}"; then
    echo "Setting up dual output on '${_1}' and '${_2}'..."
    a xrandr_retry ${disable:+--output "${_4}" --off}                        \
                   ${disable:+--output "${_3}" --off}                        \
                   --output "${_2}" "${@}"                                   \
                   --output "${_1}" "${@}" --primary --right-of "${_2}"      \
                   --dpi    "${_1}" &&
      a xrdb_set Xft.dpi "$(xdpyinfo_resolution_y)"
  else
    return "${?}"
  fi
}
##
xrandr_mono_output() {
  for i in 1 2 3 4; do
    eval _${i}="\"\${${1}-}\""
    eval _${i}_status_connected="\"\${${1}_status_connected-}\""
    shift
  done
  if [ -n "${_1_status_connected}" ]   &&                                    \
     xrandr_wait_output_status "${_1}"; then
    echo "Setting up mono output on '${_1}'..."
    a xrandr_retry ${disable:+--output "${_4}" --off}                        \
                   ${disable:+--output "${_3}" --off}                        \
                   ${disable:+--output "${_2}" --off}                        \
                   --output "${_1}" "${@}" --primary                         \
                   --dpi    "${_1}" &&
      a xrdb_set Xft.dpi "$(xdpyinfo_resolution_y)"
  else
    return "${?}"
  fi
}
##
xrdb_set() {
  a [ -n "${1-}" ] && a [ -n "${2-}" ] || return "${?}"
  echo "${1}: ${2}" | a xrdb -merge -nocpp
}
##  ==========================================================================
##  }}} Functions
##
### Variables {{{
##  ==========================================================================
xrandr_wait_count=10
xrandr_wait_interval=0.1
xrandr_retry_count=10
xrandr_retry_interval=1
##
disable=
enable=1
option="${enable:+auto}"
option="${option:-preferred}"
##
summary="${logprefix}"
if [ -n "${UDEV_DEVPATH-}" ] && [ -n "${UDEV_EVENT-}" ]; then
  summary="${summary} [udev-${UDEV_EVENT}]"
fi
notifiable=
if q command_name notify-send            &&                                  \
   [ -n "${DBUS_SESSION_BUS_ADDRESS-}" ] &&                                  \
   [ -n "${DISPLAY-}"                  ] &&                                  \
   [ -n "${HOME-}"                     ] &&                                  \
   [ -n "${USER-}"                     ] &&                                  \
   [ -n "${XAUTHORITY-}"               ]; then
  notifiable=1
fi
statuses="$(echo /sys${UDEV_DEVPATH:-/class/drm}/*/status)"
for status in ${statuses}; do
  device="${status%/status}"
  device="${device##*/}"
  kernel="${device%%-*}"
  output="${device#*-}"
  status="$(trim "$(cat -- "${status}")")"
  message="Device '${kernel}' for output '${output}':"
  message="${message} Changed status to '${status}'"
  if [ -n "${notifiable}" ]; then
    notify-send -t 10000 --urgency='low' "${summary}" "${message}"
  fi
  echo "${message}"
  output="${output%%-*}"
  if [ -n "${output}" ]; then
    eval ${output}="\"\${${output}:-${output}-${kernel#card}}\""
    if [ -n "${status}" ]; then
      eval ${output}_status_connected=
      eval ${output}_status_disconnected=
      eval ${output}_status_${status}=1
      eval ${output}_status="'${status}'"
    fi
  fi
done
unset -v device
unset -v kernel
unset -v message
unset -v notifiable
unset -v output
unset -v status
unset -v statuses
unset -v summary
##  ==========================================================================
##  }}} Variables
##
### Dual Outputs {{{
##  ==========================================================================
xrandr_dual_output DP   HDMI VGA  LVDS "--${option}" ||                      \
xrandr_dual_output DP   VGA  HDMI LVDS "--${option}" ||                      \
xrandr_dual_output DP   LVDS HDMI VGA  "--${option}" ||                      \
xrandr_dual_output HDMI VGA  DP   LVDS "--${option}" ||                      \
xrandr_dual_output HDMI LVDS DP   VGA  "--${option}" ||                      \
xrandr_dual_output VGA  LVDS DP   HDMI "--${option}" ||                      \
xrandr_mono_output DP   HDMI VGA  LVDS "--${option}" ||                      \
xrandr_mono_output HDMI VGA  LVDS DP   "--${option}" ||                      \
xrandr_mono_output VGA  LVDS DP   HDMI "--${option}" ||                      \
xrandr_mono_output LVDS DP   HDMI VGA  "--${option}" ||                      \
echo "Using default output..."
##  ==========================================================================
##  }}} Dual Outputs
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: End"
##  ==========================================================================
##  }}} Messages
##
### References {{{
##  ==========================================================================
##  [1] file:///usr/local/bin/udev-monitor-hotplug
##  ==========================================================================
##  }}} References
