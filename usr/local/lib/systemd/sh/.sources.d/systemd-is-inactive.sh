#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file systemd-is-inactive.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-03-29 Tuesday 14:35:14 (+0200)
##  --------------------------------------------------------------------------
##     @created 2021-04-27 Tuesday 22:47:03 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Functions
##
### Variables {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Variables
##
### Commands {{{
##  ==========================================================================
! systemctl is-active "${@}"
##  ==========================================================================
##  }}} Commands
