#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file killall-wait.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-12-18 Saturday 18:03:28 (+0100)
##  --------------------------------------------------------------------------
##     @created 2021-04-27 Tuesday 22:47:03 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
duration() {
  systemctl show --property=DefaultTimeoutStopUSec --value
}
##
killall_wait() {
  ${duration:+timeout -k "${duration}" "${duration}"} killall -w "${@}" || (
    status="${?}"
    if [ "${status}" -eq 124 ] || [ "${status}" -eq 137 ]; then
      exec killall -KILL -w "${@}"
    fi
    return "${status}"
  )
}
##  ==========================================================================
##  }}} Functions
##
### Variables {{{
##  ==========================================================================
duration="${KILLALL_WAIT_TIMEOUT_DURATION-$({
    duration || echo 10
  } | tr -d ' ')}"
duration="${duration#infinity}"
if [ "${duration}" != "${duration%%[![:digit:]]*}" ]; then
  duration="${duration%${duration#*[![:digit:]]}}"
fi
##  ==========================================================================
##  }}} Variables
##
### Commands {{{
##  ==========================================================================
killall_wait "${@}" && while killall_wait "${@}"; do
  :
done
##  ==========================================================================
##  }}} Commands
