### Preamble {{{
##  ==========================================================================
##        @file commands.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-02-16 Tuesday 00:49:31 (+0100)
##  --------------------------------------------------------------------------
##     @created 2021-02-14 Sunday 12:00:12 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Functions {{{
##  ==========================================================================
session_pid() {
  set --
  if [ -n "${SESSION_MANAGER-}" ]; then
    set -- "${SESSION_MANAGER##*/}"
  fi
  if [ -n "${UPSTART_SESSION-}" ]; then
    set -- "${UPSTART_SESSION##*/}"
  fi
  if [ "${1-}" -gt 0 ]; then
    echo "${1}"
  else
    return "${?}"
  fi
}
##
session_path() {
  set -- "${1-$(session_pid)}"
  if [ "${1}" -gt 0 ]; then
    set -- "${1}" 'org.freedesktop.login1' '/org/freedesktop/login1'
    set -- "$(gdbus call -y                                                  \
                         -d "${2}"                                           \
                         -o "${3}"                                           \
                         -m "${2}.Manager.GetSessionByPID" "${1}")" "${?}"
    if [ "${2}" -eq 0 ]; then
      set -- "${1#*\'}" "${1}"
      set -- "${1%\'*}" "${2}"
      if [ -n "${1}" ] && [ "${1}" != "${2}" ]; then
        echo "${1}"
      else
        return "${?}"
      fi
    else
      return "${2}"
    fi
  else
    return "${?}"
  fi
}
##
session_name() {
  set -- "${1-$(session_path)}"
  set -- "${1#/org/freedesktop/login1/session/}" "${1}"
  if [ -n "${1}" ] && [ "${1}" != "${2}" ]; then
    echo "${1}"
  else
    return "${?}"
  fi
}
##
user_path() {
  set -- "${1-$(session_pid)}"
  if [ "${1}" -gt 0 ]; then
    set -- "${1}" 'org.freedesktop.login1' '/org/freedesktop/login1'
    set -- "$(gdbus call -y                                                  \
                         -d "${2}"                                           \
                         -o "${3}"                                           \
                         -m "${2}.Manager.GetUserByPID" "${1}")" "${?}"
    if [ "${2}" -eq 0 ]; then
      set -- "${1#*\'}" "${1}"
      set -- "${1%\'*}" "${2}"
      if [ -n "${1}" ] && [ "${1}" != "${2}" ]; then
        echo "${1}"
      else
        return "${?}"
      fi
    else
      return "${2}"
    fi
  else
    return "${?}"
  fi
}
##
user_name() {
  set -- "${1-$(user_path)}"
  set -- "${1#/org/freedesktop/login1/user/}" "${1}"
  if [ -n "${1}" ] && [ "${1}" != "${2}" ]; then
    echo "${1}"
  else
    return "${?}"
  fi
}
##  ==========================================================================
##  }}} Functions
