#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file dbus-logind-monitor.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-12-18 Saturday 16:37:32 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-05-22 Friday 15:04:25 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh" &&                    \
  . "${__REALFILEDIR__%/}/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Traps {{{
##  ==========================================================================
a trap_or_exit -- :
##  ==========================================================================
##  }}} Traps
##
### Variables {{{
##  ==========================================================================
logprefix="D-Bus Login Monitor"
##  ==========================================================================
##  }}} Variables
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: Begin"
##  ==========================================================================
##  }}} Messages
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##
DBUS_LOGIND_CONFIG_HOME="$(xdg_config_home)/dbus/logind"
DBUS_LOGIND_ENVIRONMENT_FILE="${DBUS_LOGIND_CONFIG_HOME}/logind.conf"
DBUS_LOGIND_MONITOR_FILE="${DBUS_LOGIND_CONFIG_HOME}/monitor"
DBUS_LOGIND_SESSION_NAME="$(session_name)"
DBUS_LOGIND_USER_NAME="$(user_name)"
##
name='org.freedesktop.login1'
path='/org/freedesktop/login1'
if [ "${#}" -gt 0 ]; then
  case "${1}" in
    -s|--session)
      session_name="${2:-${DBUS_LOGIND_SESSION_NAME}}"
      session_name="${session_name#/}"
      session_name="${session_name%/}"
      a [ -n "${session_name}" ]
      relpath="session/${session_name}"
      ;;
    -u|--user)
      user_name="${2:-${DBUS_LOGIND_USER_NAME}}"
      user_name="${user_name#/}"
      user_name="${user_name%/}"
      a [ -n "${user_name}" ]
      relpath="user/${user_name}"
      ;;
    *)
      session_name=
      user_name=
      relpath="${1}"
      relpath="${relpath#/}"
      relpath="${relpath%/}"
      ;;
  esac
fi
abspath="${path}${relpath:+/}${relpath}"
#
notifiable=
if q command_name notify-send            &&                                  \
   [ -n "${DBUS_SESSION_BUS_ADDRESS-}" ] &&                                  \
   [ -n "${DISPLAY-}"                  ] &&                                  \
   [ -n "${HOME-}"                     ] &&                                  \
   [ -n "${USER-}"                     ] &&                                  \
   [ -n "${XAUTHORITY-}"               ]; then
  notifiable=1
fi
##  ==========================================================================
##  }}} Variables
##
### Exports {{{
##  ==========================================================================
export DBUS_LOGIND_CONFIG_HOME
export DBUS_LOGIND_ENVIRONMENT_FILE
export DBUS_LOGIND_MONITOR_FILE
export DBUS_LOGIND_SESSION_NAME
export DBUS_LOGIND_USER_NAME
##  ==========================================================================
##  }}} Exports
##
### Messages {{{
##  ==========================================================================
echo "D-Bus Login Environment:"
export -p | cut -d ' ' -f 2- | grep -e '^DBUS_LOGIND_'
echo "User Environment:"
echo "DBUS_SESSION_BUS_ADDRESS='${DBUS_SESSION_BUS_ADDRESS-}'"
echo "DISPLAY='${DISPLAY-}'"
echo "HOME='${HOME-}'"
echo "USER='${USER-}'"
echo "XAUTHORITY='${XAUTHORITY-}'"
##  ==========================================================================
##  }}} Messages
##
### Monitors {{{
##  ==========================================================================
{
  gdbus monitor -y -d "${name}" -o "${abspath}" || killpg "${PID}"
} | while IFS= read -r string; do
  echo "${string}"
  signal="${string#${path}*:*[[:blank:]]${name}.}"
  if [ "${signal}" = "${string}" ]; then
    continue
  fi
  signal="$(trim "${signal%%(*}")"
  interface="${signal%.*}"
  signal="${signal##*.}"
  if [ -z "${interface}" ] || [ -z "${signal}" ]; then
    continue
  fi
  abspath="${string%%:*}"
  echo "Received signal '${signal}'"                                         \
       "of interface '${name}.${interface}'"                                 \
       "for object path '${abspath}'"
  relpath="${abspath#${path}}"
  relpath="${relpath#/}"
  relpath="${relpath%/}"
  message="Interface '${interface}'${relpath:+ for object '${relpath}'}:"
  message="${message} Received signal '${signal}'"
  if [ -n "${notifiable}" ]; then
    notify-send -t 10000 --urgency='low' "${logprefix}" "${message}"
  fi
  echo "${message}"
  for _monitor in "${DBUS_LOGIND_MONITOR_FILE}"                              \
                  "${DBUS_LOGIND_MONITOR_FILE}."*                            \
                  "${DBUS_LOGIND_MONITOR_FILE}.d/"*; do
    {
      set -- "${_monitor}" "${relpath}" "${interface}" "${signal}"
      if   [ ! -f "${1}" ]; then
        continue
      elif [   -x "${1}" ]; then
        :
      elif [   -r "${1}" ] && [ "${1##*.}" = 'sh' ]; then
        set -- env sh "${@}"
      else
        continue
      fi
      echo "Running '${1}'..."
      a "${@}" || :
    }
  done
  unset -v _monitor
done
##  ==========================================================================
##  }}} Monitors
##
### Messages {{{
##  ==========================================================================
echo "${logprefix}: End"
##  ==========================================================================
##  }}} Messages
