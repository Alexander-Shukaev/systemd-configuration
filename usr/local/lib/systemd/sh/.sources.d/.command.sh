#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file .command.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-03-23 Tuesday 22:51:50 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-08-28 Friday 16:51:38 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Internal Variables {{{
##  ==========================================================================
       __FILE__="${__REALFILEDIR__%/}/${__REALLINK__##*/}"
   __REALFILE__="$(__realpath__           "${__FILE__}")"
__REALFILEDIR__="$(command dirname -- "${__REALFILE__}")"
##  ==========================================================================
##  }}} Internal Variables
##
### Checks {{{
##  ==========================================================================
if [ "${__realfile__}" =     "${__FILE__}" ] ||                              \
   [ "${__realfile__}" = "${__REALFILE__}" ]; then
  printf -- '%s: %s: %s\n' 'error' "${__link__}" 'Invalid symbolic link' 1>&2
  exit 1
fi
if [ "${SYSTEMD_TARGET-}" = "${__REALFILE__}" ]; then
  printf -- '%s: %s: %s\n' 'error' "${__link__}" 'Recursive execution'   1>&2
  exit 1
fi
##  ==========================================================================
##  }}} Checks
##
### Variables {{{
##  ==========================================================================
SYSTEMD_SOURCE="${__FILE__}"
SYSTEMD_TARGET="${__REALFILE__}"
##  ==========================================================================
##  }}} Variables
##
### Exports {{{
##  ==========================================================================
export SYSTEMD_TARGET
##  ==========================================================================
##  }}} Exports
##
### Sources {{{
##  ==========================================================================
. "${__REALFILE__}"
##  ==========================================================================
##  }}} Sources
