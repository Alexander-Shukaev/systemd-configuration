### Preamble {{{
##  ==========================================================================
##        @file bw-get-attachment.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-01-10 Monday 13:39:19 (+0100)
##  --------------------------------------------------------------------------
##     @created 2021-10-17 Sunday 13:41:35 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
bw() {
  command bw --nointeraction "${@}"
}
##
bw_get_attachment() {
  a [ "${#}" -le 3 ]                    || return "${?}"
  [ "${#}" -lt 3 ] || a rm -f -- "${3}" || return "${?}"
  ${duration:+timeout -k "${duration}" "${duration}"}                        \
    bw get ${2+--itemid   "${2}"}                                            \
           ${3+--output   "${3}"}                                            \
           ${1+attachment "${1}"} && {
    [ "${#}" -lt 3 ] || [ -f "${3}" ]
  }
}
##  ==========================================================================
##  }}} Functions
##
### Variables {{{
##  ==========================================================================
duration="${BW_GET_ATTACHMENT_TIMEOUT_DURATION-60}"
duration="${duration#infinity}"
if [ "${duration}" != "${duration%%[![:digit:]]*}" ]; then
  duration="${duration%${duration#*[![:digit:]]}}"
fi
##  ==========================================================================
##  }}} Variables
##
### Commands {{{
##  ==========================================================================
a [ "${#}" -le 3 ] && retry -i 0 -- bw_get_attachment "${@}"
##  ==========================================================================
##  }}} Commands
