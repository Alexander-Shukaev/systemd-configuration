#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file systemd-environment.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-02-25 Thursday 12:58:21 (+0100)
##  --------------------------------------------------------------------------
##     @created 2020-08-28 Friday 16:51:38 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Functions {{{
##  ==========================================================================
environment() {
  export -p | cut -d ' ' -f 2-
}
##
systemd_environment() {
  PAGER='cat' systemctl "${@}" 'show-environment'
}
##
main() {
  case "${#}" in
    0)
      exec 'systemctl' 'show-environment'
      ;;
    1)
      action="${1}"
      option=
      if [ -z "${action}" ]; then
        (>&2 'echo' "${cmd}: Empty action name")
        return 1
      fi
      case "${action}" in
        -u|--user)
          exec 'systemctl' --user 'show-environment'
          ;;
      esac
      ;;
    2)
      action="${1}"
      option="${2}"
      if [ -z "${action}" ]; then
        (>&2 'echo' "${cmd}: Empty action name")
        return 1
      fi
      if [ -z "${option}" ]; then
        (>&2 'echo' "${cmd}: Empty option name")
        return 1
      fi
      case "${option}" in
        -u|--user)
          option='--user'
          ;;
        *)
          (>&2 'echo' "${cmd}: No such option name: ${option}")
          return 1
          ;;
      esac
      ;;
    *)
      (>&2 'echo' "${cmd}: Too many arguments: ${@}")
      return 1
      ;;
  esac
  case "${action}" in
    set)
      args="$(environment | grep -F '=' | grep -e '^SHLVL=' -e '^TERM=' -v)"
      ;;
    unset)
      args="$(systemd_environment ${option} | cut -d '=' -f 1)"
      ;;
    *)
      (>&2 'echo' "${cmd}: No such action name: ${action}")
      return 1
      ;;
  esac
  ifs="${IFS}"; ${IFS+:} unset -v ifs
  IFS='
'
  set -f
  set -- ${args}
  set +f
  IFS="${ifs}"; ${ifs+:} unset -v IFS
  eval exec 'systemctl' ${option} "${action}-environment" ${@}
}
##  ==========================================================================
##  }}} Functions
##
### Main {{{
##  ==========================================================================
cmd="${0##*/}"
main "${@}"
##  ==========================================================================
##  }}} Main
