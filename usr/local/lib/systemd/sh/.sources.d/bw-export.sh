### Preamble {{{
##  ==========================================================================
##        @file bw-export.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2022-05-05 Thursday 01:21:12 (+0200)
##  --------------------------------------------------------------------------
##     @created 2021-10-17 Sunday 13:41:35 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2022,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Sources {{{
##  ==========================================================================
SHELL_COMMANDS_DEFINE_SHORTCUT_FUNCTIONS=1                                   \
  . "${__REALLINKDIR__%/}/../../../lib/sh/commands.sh"
##  ==========================================================================
##  }}} Sources
##
### Locks {{{
##  ==========================================================================
a lock -c -a -e -p "${0}" -v -- "${0}" "${@}"
##  ==========================================================================
##  }}} Locks
##
### Variables {{{
##  ==========================================================================
a getpid PID && a [ "${PID}" -eq "${$}" ]
##
user="${1-}"
##
dir="${2-}"
dir="${dir%/}"
##
attachments_dir="${dir:+${dir}/}attachments"
vault_file="${dir:+${dir}/}vault.json"
##
clientsecret="$(a bw-clientsecret user "${user}")" || exit "${?}"
    password="$(a bw-password     user "${user}")" || exit "${?}"
##
filter="$(a printf -- '%s\\u0000'                                            \
                      '\(.id)'                                               \
                      '\($parent.id)'                                        \
                      "${attachments_dir}"'/\($parent.id)/\(.fileName)')"
filter="$(a printf -- '%s | '                                                \
                      '.[]'                                                  \
                      'select(.attachments != null)'                         \
                      '. as $parent'                                         \
                      '.attachments[]')\"${filter}\""
##  ==========================================================================
##  }}} Variables
##
### Functions {{{
##  ==========================================================================
bw() {
  command bw --nointeraction "${@}"
}
##
bw_export() {
  bw export --format json --output "${@}"
}
##
bw_login() {
  BW_CLIENTID="${1-}" BW_CLIENTSECRET="${2-}" bw login --apikey --raw
}
##
bw_session() {
  BW_SESSION="$(P="${1-}" a bw unlock ${1+--passwordenv P} --raw)" &&
    export BW_SESSION
}
##
bw_sync() {
  ${duration:+timeout -k "${duration}" "${duration}"} bw sync "${@}"
}
##  ==========================================================================
##  }}} Functions
##
### Variables {{{
##  ==========================================================================
duration="${BW_SYNC_TIMEOUT_DURATION-60}"
duration="${duration#infinity}"
if [ "${duration}" != "${duration%%[![:digit:]]*}" ]; then
  duration="${duration%${duration#*[![:digit:]]}}"
fi
##  ==========================================================================
##  }}} Variables
##
### Commands {{{
##  ==========================================================================
q bw_login "user.${user}" "${clientsecret}" || :
bw_session "${password}"                    || exit "${?}"
a retry -i 0 -- bw_sync                     || exit "${?}"
q bw_export "${vault_file}" "${password}"   || exit "${?}"
a bw list items | a jq -j -r "${filter}" |
  q xargs -0 -n 3 -P 2 -- bw-get-attachment
##  ==========================================================================
##  }}} Commands
